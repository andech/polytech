import React, { Component } from "react";
import "./SingleTask.css";


window.setResponse = function(data){
    window.userId = data.response[0].id;
    window.VK.Widgets.ContactUs("vk_contact_us", {text: "Написать куратору"}, window.userId);
    
};
export class SingleTask extends Component {
    constructor(props){
        super(props);
        this.props = props;
        this.state = {
            task: {}
        };
    }
    componentDidMount(){
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}`)
        .then(response => response.json())
        .then(task => this.setState({task}));

        var script = document.createElement('SCRIPT');
        script.src = `https://api.vk.com/method/users.get?user_ids=ivcatherine&fields=bdate&access_token=62b20d4062b20d4062b20d40be62df2d79662b262b20d403f3d8e356e26a94154d27e89&v=5.101&callback=setResponse`;
        document.getElementsByTagName("head")[0].appendChild(script);
    }
    render() {
        return (
            <div className="task__container">
                <h1>{this.state.task.title}</h1>
                <div>{this.state.task.body}</div>
                {window.userId ? (
                <div id="vk_contact_us">
                </div>) : (<div></div>)
                }
            </div>
           );
    };
}