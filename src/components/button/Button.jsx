import React, { Component } from "react";
import "./Button.css"

export class Button extends Component {
    render() {
        const { className = "defaultButton"} = this.props;
        return (
            <button
                className={className}
                onClick={this.props.onClick}
            >
                {this.props.text}
            </button>);
    };
}