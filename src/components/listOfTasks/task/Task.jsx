import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "../../button/Button";

import "./Task.css";

export class Task extends Component {
    isVisible = false;

    toggleModal = () => {
        this.isVisible = true;
    }
    render() {
        return (
            <div className="task task_color">
                <Link to={`/tasks/${this.props.id}`}>
                    <div className="task_description">
                            <h2 className="task__header2">Название задачи</h2>
                            <div>Описание: {this.props.value}</div>
                            <div>Дата</div>
                    </div>
                </Link>
                <Button className="task_button button_margin" onClick={this.toggleModal} text="Принять участие"></Button>
                {this.isVisible && <div>dGETGEGESRH</div>}
            </div>

        );
    };
}