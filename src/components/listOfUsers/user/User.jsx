import React, { Component } from "react";
import { Button } from "../../button/Button";

import "../../listOfTasks/task/Task.css";

export class User extends Component {
    render() {
        return (
            <div className="task task_color">
                    <div className="task_description">
                            <h2 className="task__header2">{this.props.name}</h2>
                            <div>{this.props.email}</div>
                            <div>Рейтинг: 3.5</div>
                    </div>
                    <Button className="user_button button_margin" text="Поставить оценку"></Button>
            </div>
           );
    };
}