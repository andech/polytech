import React, { Component } from "react";
import { Link } from "react-router-dom";
import {withCookies} from 'react-cookie';

import { MenuItem } from "../menuItem/MenuItem";
import { Button } from "../button/Button"
import logo from "../../images/logo.png";
import "./Header.css";

export class Header extends Component {

    render() {
        return (<header>
                    <Link className="logo" to="/">
                        <img 
                        className="logo_size"
                        src={logo} 
                        alt="logo"></img>
                        <span>Волонтёры</span>
                    </Link>
                    <MenuItem
                        name="Задания"
                        url="/tasks"
                    ></MenuItem>
                    <MenuItem
                        name="Участники"
                        url="/members"
                    ></MenuItem>
                    <Link className="menu-item__margin" to="/login">
                        <Button
                            text={this.props.isLoggedIn ? "Выйти" : "Войти"}
                
                        ></Button>
                    </Link>
                </header>
       );
    };
}