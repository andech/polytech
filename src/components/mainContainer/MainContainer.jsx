import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import {Main} from "../main/Main";
import {Admin} from "../admin/Admin";
import { NewUserForm } from "../newUserForm/NewUserForm";
import { NewMuseumForm } from "../newMuseumForm/NewMuseumForm";
import { ListOfTasks } from "../listOfTasks/ListOfTasks";
import { ListOfUsers } from "../listOfUsers/ListOfUsers";
import {Header} from  '../header/Header';
import {Footer} from  '../footer/Footer';
import { VolunteerForm } from '../volunteerForm/VolunteerForm';
import { NewTask } from "../newTask/NewTask"

import "./MainContainer.css";
import { AdminLoginForm } from "../adminLoginForm/AdminLoginForm";
import { SingleTask } from "../singleTask/SingleTask"

export class MainContainer extends Component {
    render() {
        return (
            <Router>
              <div className="container">
                    <Header isLoggedIn={this.props.isLoggedIn}></Header>
                    <Route exact path="/" component={Main} />
                    <Route exact path="/registration" component={VolunteerForm} />
                    <Route exact path="/login" component={AdminLoginForm} handleLogin={this.props.handleLogin}/>
                    <Route path="/admin" component={Admin} />
                    <Route path="/newUser" component={NewUserForm}/>
                    <Route exact path="/newMuseum" component={NewMuseumForm} />
                    <Route exact path="/tasks" component={ListOfTasks}/>
                    <Route exact path="/members" component={ListOfUsers}/>
                    <Route exact path="/tasks/:id" component={SingleTask}/>
                    <Route exact path="/newTask" component={NewTask}/>
                    <Footer></Footer>
              </div>
            </Router>);
    };
}
