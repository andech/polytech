import React, { Component } from "react";
import { Button } from "../button/Button";
import "./NewMuseumForm.css";

import { SERVER_URL, REGISTER_ORGANIZATION_PATH } from '../../constants';

export class NewMuseumForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            organization: {
                name: '',
                address: ''
            }
        };
    }

    handleRegisterSubmit = (event) => {

        event.preventDefault();

        fetch(`${SERVER_URL}${REGISTER_ORGANIZATION_PATH}`, {
            method: 'POST',
            body: JSON.stringify(this.state)
        });
        
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleAdditionalInput = (event) => {
        this.setState({
            organization: { 
                ...this.state.organization,
                [event.target.name]: event.target.value
            }
        });
    }

    render() {
        return (
            <form className="form_display" onSubmit={this.handleRegisterSubmit}>

                <label>
                    Логин:
                    <input 
                        type="text" 
                        name="login"
                        className="for" 
                        value={this.state.login} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    Пароль:
                    <input 
                        type="password" 
                        name="password" 
                        value={this.state.password} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    Название организации:
                    <input 
                        type="text" 
                        name="name" 
                        value={this.state.organization.name} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Адрес:
                    <input 
                        type="text" 
                        name="address" 
                        value={this.state.organization.address} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <Button 
                    text="Зарегистрировать"
                />

            </form>
        );
    };
}