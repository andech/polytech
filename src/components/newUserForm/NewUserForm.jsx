import React, { Component } from "react";
import { Button } from "../button/Button";
import "./NewUserForm.css";

import { SERVER_URL, REGISTER_PERSON_PATH } from '../../constants';

export class NewUserForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            person: {
                name: '',
                age: ''
            }
        };
    }

    handleRegisterSubmit = (event) => {

        event.preventDefault();

        fetch(`${SERVER_URL}${REGISTER_PERSON_PATH}`, {
            method: 'POST',
            body: JSON.stringify(this.state)
        });
        
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleAdditionalInput = (event) => {
        this.setState({
            person: { 
                ...this.state.person,
                [event.target.name]: event.target.value
            }
        });
    }

    render() {
        return (
            <form className="form_display" onSubmit={this.handleRegisterSubmit}>

                <label>
                    Логин:
                    <input 
                        type="text" 
                        name="login"
                        className="for" 
                        value={this.state.login} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    Пароль:
                    <input 
                        type="password" 
                        name="password" 
                        value={this.state.password} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    ФИО:
                    <input 
                        type="text" 
                        name="name" 
                        value={this.state.person.name} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Возраст:
                    <input 
                        type="number" 
                        name="age"
                        min="0"
                        max="200"
                        value={this.state.person.age} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <Button 
                    text="Зарегистрировать"
                />

            </form>
        );
    };
}