import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./MenuItem.css";

export class MenuItem extends Component {
    render() {
        const { className="" } = this.props;
        return (
            <span className={`${className} header__menuItem`}>
                <Link to={this.props.url}>
                    {this.props.name}
                </Link>
            </span>
           );
    };
}