import React, { Component } from "react";
import { User } from "../listOfUsers/user/User";
import { Button } from "../button/Button"

import "../listOfTasks/ListOfTasks.css";
import "./ListOfUsers.css";

export class ListOfUsers extends Component {
    constructor(props){
        super(props);
        this.props = props;
        this.state = {
            users: [],
            allUsers: []
        };
    }
    onChange = (event) => {
        this.setState({
            searchParam: event.target.value
          });
    }

    onSearch = (event) => {
        event.preventDefault();
        if (this.state.searchParam !== "" && this.state.searchParam !== undefined) {
            this.setState({
                users: this.state.allUsers.filter(item => item.name.includes(this.state.searchParam))
            })
        } else {
            this.setState({
                users: this.state.allUsers
            })
        }
        
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(allUsers => {
            this.setState({allUsers});
            this.setState({
                users : allUsers
            });
        });
        
    }
    render() {
        return (
            <div className="list-of-users">
                <div className="list-of-users__filters">
                    <div className="filters">
                        <form onSubmit={this.onSearch} className="filters__form">
                                <input className="filters__input" onChange={this.onChange} type="text"></input>
                            <Button text="Найти"></Button>
                        </form>
                    </div>
                </div>
                <div className="list-of-tasks__list">
                    <h1>Участники</h1>
                    {this.state.users.map(user => (
                        <User 
                            id={user.id}
                            key={user.id} 
                            name={user.name}
                            email={user.email}>
                        </User>
                    ))}
                </div>
           </div>
           );
    };
}