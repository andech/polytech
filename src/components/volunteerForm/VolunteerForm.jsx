import React, { Component } from "react";
import { Button } from "../button/Button";
import "./VolunteerForm.css";

import { SERVER_URL, REGISTER_PERSON_PATH } from '../../constants';

export class VolunteerForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            profile: {
                fullName: '',
                birthdate: '',
                phone: '',
                socialLink: '',
                workingPlace: '',
                specialty: '',
                foreignLanguages: '',
                volunteerExperience: '',
                projectsOfInterest: [],
                workWithChildrenExperience: '',
                skills: '',
                expectations: '',
                medicalСontraindications: '',
                foodPreferences: '',
                animalAllergy: '',
                clothSize: '',
                photoLink: '',
                whereHearAbout: '',
                whyInterestInСooperation: '',
                isReceiveNews: '',
                personalDataProcessing: ''
            }
        };
    }

    handleRegisterSubmit = (event) => {

        event.preventDefault();

        fetch(`${SERVER_URL}${REGISTER_PERSON_PATH}`, {
            method: 'POST',
            body: JSON.stringify(this.state)
        });
        
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleAdditionalInput = (event) => {
        this.setState({
            profile: { 
                ...this.state.profile,
                [event.target.name]: event.target.value
            }
        });
    }

    render() {
        return (
            <form className="profile-form form_display" onSubmit={this.handleRegisterSubmit}>

                <label>
                    Email:
                    <input 
                        type="email" 
                        name="email" 
                        value={this.state.email} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Пароль:
                    <input 
                        type="password" 
                        name="password" 
                        value={this.state.password} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    ФИО:
                    <input 
                        type="text" 
                        name="fullName" 
                        value={this.state.profile.fullName} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Дата рождения:
                    <input 
                        type="date" 
                        name="birthdate" 
                        value={this.state.profile.birthdate} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Номер телефона:
                    <input 
                        type="tel"
                        name="phone" 
                        value={this.state.profile.phone} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Ссылка на личную страницу ВК или Fb:
                    <input 
                        type="text" 
                        name="socialLink" 
                        value={this.state.profile.socialLink} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Место учебы/ работы:
                    <input 
                        type="text" 
                        name="workingPlace" 
                        value={this.state.profile.workingPlace} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Специальность по диплому:
                    <input 
                        type="text" 
                        name="specialty" 
                        value={this.state.profile.specialty} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Какими иностранными языками Вы владеете (укажите уровень владения)?:
                    <input 
                        type="text" 
                        name="foreignLanguages" 
                        value={this.state.profile.foreignLanguages} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Есть ли у вас опыт волонтерства? Если есть, то опишите его. Если нет - это не страшно)):
                    <input 
                        type="text" 
                        name="volunteerExperience" 
                        value={this.state.profile.volunteerExperience} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Какие проекты Политеха Вам наиболее интересны?:
                    <input 
                        type="text" 
                        name="projectsOfInterest" 
                        value={this.state.profile.projectsOfInterest} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Есть ли у вас опыт работы с детьми? Если есть, то опишите его:
                    <input 
                        type="text" 
                        name="workWithChildrenExperience" 
                        value={this.state.profile.workWithChildrenExperience} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Какие дополнительные навыки могут быть полезны в сотрудничестве с Политехом? (возможно, вы прекрасно фотографируете или умеете красиво и профессионально говорить со сцены, напишите об этом!):
                    <input 
                        type="text" 
                        name="skills" 
                        value={this.state.profile.skills} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Какие ожидания у вас от волонтерства в проектах Политехнического музея? Что волонтерство может дать лично вам?:
                    <input 
                        type="text" 
                        name="expectations" 
                        value={this.state.profile.expectations} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Есть ли у Вас медицинские противопоказания, аллергия, в т.ч. на животных?:
                    <input 
                        type="text" 
                        name="medicalСontraindications" 
                        value={this.state.profile.medicalСontraindications} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Есть ли у Вас предпочтения в еде?:
                    <input 
                        type="text" 
                        name="foodPreferences" 
                        value={this.state.profile.foodPreferences} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Размер одежды:
                    <input 
                        type="text" 
                        name="clothSize" 
                        value={this.state.profile.clothSize} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Ссылка на Ваше фото:
                    <input 
                        type="text" 
                        name="photoLink" 
                        value={this.state.profile.photoLink} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Откуда вы узнали о нас?:
                    <input 
                        type="text" 
                        name="whereHearAbout" 
                        value={this.state.profile.whereHearAbout} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Почему Вам интересно сотрудничать с Политехническим музеем?:
                    <input 
                        type="text" 
                        name="whyInterestInСooperation" 
                        value={this.state.profile.whyInterestInСooperation} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Вы хотите получать новости от Политеха?:
                    <input 
                        type="text" 
                        name="isReceiveNews" 
                        value={this.state.profile.isReceiveNews} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <label>
                    Согласие на обработку персональных данных в соответствие с Федеральным законом РФ от 27 июля 2006 года № 152-ФЗ «О персональных данных» *Если вы не даете согласия на обработку данных, то в соответствии с законом, все предоставленные вами данные будут немедленно удалены, и вы не будете внесены в список участников конференции:
                    <input 
                        type="text" 
                        name="personalDataProcessing" 
                        value={this.state.profile.personalDataProcessing} 
                        onChange={this.handleAdditionalInput}
                        required
                    />
                </label>

                <Button 
                    text="Зарегистрировать"
                />

            </form>
        );
    };
}