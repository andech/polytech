import React, { Component } from "react";
import {Button} from "../button/Button";
import {Link } from "react-router-dom";
import "./Admin.css"

export class Admin extends Component {
    createNewMuseum = () => {
    }
    render() {
        return (<div className="button-group button-group_display">
            <Link 
                className="create-user-link"
                to="/newUser">
                <Button
                    text="Создать пользователя"
                >
                </Button>
            </Link>
            <Link to="/newMuseum">
                <Button
                    text="Создать организацию"
                >
                </Button>
            </Link>
        </div>);
    };
}