import React, { Component } from "react";
import { Button } from "../button/Button";

export class NewTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            curatorId: 2,
            title: '',
            description: '',
            destination: "Политехнический музей",
            date: '',
            taskId: 0,
            volunteerId: ""
        };
    }

    handleRegisterSubmit = (event) => {

        event.preventDefault();

        fetch(`http://95.213.37.5:8080/v1/task`, {
            method: 'POST',
            body: JSON.stringify(this.state),
            headers: {
                "Content-type": "application/json"
            }
        });
        
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    render() {
        return (
            <form className="form_display" onSubmit={this.handleRegisterSubmit}>
                <label>
                    Название:
                    <input 
                        type="text" 
                        name="title"
                        className="for" 
                        value={this.state.title} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    Описание:
                    <input 
                        type="text" 
                        name="description" 
                        value={this.state.description} 
                        onChange={this.handleInput}
                        required
                    />
                </label>

                <label>
                    Дата:
                    <input 
                        type="text" 
                        name="date" 
                        value={this.state.date} 
                        onChange={this.handleInput}
                    />
                </label>

                <label>
                    Волонтёры:
                    <input 
                        type="text" 
                        name="volunteerId"
                        value={this.state.volunteerId} 
                        onChange={this.handleInput}
                    />
                </label>

                <Button 
                    text="Создать"
                />

            </form>
        );
    };
}