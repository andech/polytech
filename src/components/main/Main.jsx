import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

import "./Main.css"
import volunteer from "../../images/volunteer.png";
import { Button } from "../button/Button";
import { Slider } from "../slider/Slider";

export class Main extends Component {
    render() {
        return (
            <Fragment>
            <div className="main-content">
                <div className="main-content__yellow-block">
                    <div className="volunteer">
                    <img 
                        className="volunteer__image_size"
                        src={volunteer} 
                        alt="volunteer">
                    </img>
                    <Link to="/registration">
                    <Button
                        className="volunteer__pink-button"
                        text="Стать волонтёром"
                    ></Button>
                    </Link>
                    </div>
                </div>
                <div className="main-content__empty-block"></div>
                <div className="main-content__text">
                    <p className="main-content__paragraph">
                        Волонтёрская программа музея объединяет 
                        более полторы тысячи неравнодушных к науке 
                        людей, оказывающих помощь во многих проектах.
                    </p>
                </div>
                
            </div>  
            <div className="super-slider">
                <Slider />
            </div>
            </Fragment>
        );
    };
}