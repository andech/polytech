import React, { Component } from "react";
import { Task } from "./task/Task";
import { Button } from "../button/Button"
import { Link } from "react-router-dom";

import "./ListOfTasks.css";

export class ListOfTasks extends Component {
    constructor(props){
        super(props);
        this.props = props;
        this.state = {
            tasks: [],
            searchParam: "all"
        };
    }
    onSearch = (event) => {
        event.preventDefault();
        if(this.state.searchParam === "all"){
            fetch('https://jsonplaceholder.typicode.com/posts')
                .then(response => response.json())
                .then(tasks => this.setState({tasks}));
        } else {
            const tasks = this.state.tasks;
            this.setState({
                tasks: tasks.filter(item => item.userId === 2)
            })
        }
    }

    handleChange = (event) => {
        this.setState({
            searchParam: event.target.value
          });
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(tasks => this.setState({tasks}));
    }
    render() {
        return (
            <div className="list-of-tasks">
                <div className="list-of-tasks__filters">
                    <div className="filters">
                        <form onSubmit={this.onSearch} className="filters__form">
                                <p><input 
                                    type="radio" 
                                    className="radio" 
                                    name="search" 
                                    value="all"
                                    onChange={this.handleChange}
                                />Показать все</p>
                                <p><input 
                                    type="radio" 
                                    className="radio" 
                                    name="search" 
                                    onChange={this.handleChange}
                                    value="my"
                                    />Показать мои</p>
                            <Button text="Найти"></Button>
                        </form>
                    </div>
                    <div  className="button__new">
                        <Link to="/newTask">
                            <Button text="Создать задание"></Button>
                        </Link>
                    </div>
                </div>
                <div className="list-of-tasks__list">
                    <h1>Задания</h1>
                    {this.state.tasks.map(task => (
                        <Task 
                            id={task.id}
                            userId={task.userId}
                            key={task.id} 
                            value={task.title}>
                        </Task>
                    ))}
                </div>
           </div>
           );
    };
}