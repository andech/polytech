import React from "react";
import { Fade } from 'react-slideshow-image';
import image_1 from '../../images/slider/1.jpg';
import image_2 from '../../images/slider/2.jpg';
import image_3 from '../../images/slider/3.jpg';
import image_4 from '../../images/slider/4.jpg';
import image_5 from '../../images/slider/5.jpg';
 
const fadeImages = [image_1, image_2, image_3, image_4, image_5];
const fadeProperties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: false,
  indicators: true,
}
 
export const Slider = () => {
  return (
    <div className="slide-container">
      <Fade {...fadeProperties}>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[0]} />
          </div>
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[1]} />
          </div>
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[2]} />
          </div>
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[3]} />
          </div>
        </div>
        <div className="each-fade">
          <div className="image-container">
            <img src={fadeImages[4]} />
          </div>
        </div>
      </Fade>
    </div>
  )
}