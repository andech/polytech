import React, { Component } from "react";
import { Button } from "../button/Button";
import "./AdminLoginForm.css";
import { Redirect } from 'react-router-dom';

import { SERVER_URL, LOGIN_PATH } from '../../constants';

export class AdminLoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    handleRegister = (event) => {

        event.preventDefault();

        let formData = new FormData();
        formData.append('email', this.state.email);
        formData.append('password', this.state.password);

        fetch(`${SERVER_URL}${LOGIN_PATH}`, {
            method: 'POST',
            mode: 'cors',
            body: formData
        })
        console.log(this.props)
        // this.props.handleLogin();
        this.props.history.push(`/`)
        
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        return (
            <form className="form_display">

                <label>
                    Логин:
                    <input 
                        type="text" 
                        name="email"
                        className="for" 
                        value={this.state.email} 
                        onChange={this.handleInput}
                    />
                </label>

                <label>
                    Пароль:
                    <input 
                        type="password" 
                        name="password" 
                        value={this.state.password} 
                        onChange={this.handleInput}
                    />
                </label>

                <Button 
                    onClick={this.handleRegister} 
                    text="Войти"
                />

            </form>
        );
    };
}